import {AuthService} from './auth.service';
import {TestBed} from '@angular/core/testing';
import {AngularFirestore} from '@angular/fire/firestore';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

describe('AuthService', () => {
  let service: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [AngularFirestore, AuthService],
      imports: [AngularFireModule.initializeApp(environment.firebase)] });
    service = TestBed.inject(AuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should user be observable', () => {
    expect(service.user).toBeInstanceOf(Observable);
  });
});
